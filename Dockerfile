FROM php:7.4-alpine

WORKDIR /usr/src/app

ARG FONTAWESOME_NPM_AUTH_TOKEN
ENV FONTAWESOME_NPM_AUTH_TOKEN=$FONTAWESOME_NPM_AUTH_TOKEN

RUN echo -e '#!/bin/sh\nphp bin/console "$@"' > /usr/local/bin/sf && chmod +x /usr/local/bin/sf

RUN apk update && apk upgrade && apk add make g++ python2 yarn gmp-dev;
RUN docker-php-ext-install pdo && docker-php-ext-install pdo_mysql && docker-php-ext-install ctype && docker-php-ext-install gmp && docker-php-ext-install bcmath;
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install;

COPY . .

EXPOSE 80

CMD php -S 0.0.0.0:80 -t public

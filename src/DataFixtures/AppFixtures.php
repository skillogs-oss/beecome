<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = (new User())
            ->setEmail('sebastien@duplessy.eu')
            ->setPlainPassword('azerty')
            ->setRoles(['ROLE_SUPER_ADMIN']);

        $manager->persist($admin);

        $manager->flush();
    }
}

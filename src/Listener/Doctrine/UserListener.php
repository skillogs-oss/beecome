<?php


namespace App\Listener\Doctrine;


use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserListener
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function prePersist(User $user, LifecycleEventArgs $eventArgs)
    {
        $this->hashPlainPassword($user);
    }

    public function preUpdate(User $user, PreUpdateEventArgs $eventArgs)
    {
        $this->hashPlainPassword($user);
    }

    private function hashPlainPassword(User $user)
    {
        if (!$user->getPlainPassword()) {
            return;
        }

        $user->setPassword(
            $this->userPasswordEncoder->encodePassword(
                $user,
                $user->getPlainPassword()
            )
        );
    }
}
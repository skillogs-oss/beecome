<?php

namespace App\Command;

use App\Service\Provider\Beecome;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BeecomeUpdateCommand extends Command
{
    protected static $defaultName = 'app:beecome:update';
    /**
     * @var Beecome
     */
    private $beecome;

    public function __construct(Beecome $beecome)
    {
        parent::__construct();
        $this->beecome = $beecome;
    }

    protected function configure()
    {
        $this->setDescription('Update users and classes from beecome');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->beecome->update();

        $io->success('The users and classes was successfully updated.');

        return Command::SUCCESS;
    }
}

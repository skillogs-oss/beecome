<?php


namespace App\Service\Provider;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Beecome
{
    /**
     * @var string
     */
    private $beecomeBaseUrl;
    /**
     * @var string
     */
    private $beecomeApiKey;
    /**
     * @var string
     */
    private $beecomeSchoolId;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(string $beecomeBaseUrl, string $beecomeApiKey, string $beecomeSchoolId, HttpClientInterface $httpClient, EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->beecomeBaseUrl = $beecomeBaseUrl;
        $this->beecomeApiKey = $beecomeApiKey;
        $this->beecomeSchoolId = $beecomeSchoolId;
        $this->httpClient = $httpClient;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function checkLogin(User $user, string $email, string $password): bool
    {
        try {
            $response = $this->httpClient->request('POST', $this->beecomeBaseUrl . '/api/v1/login_check', [
                'json' => ['email' => $email, 'password' => $password]
            ])->getContent();
        } catch (\Exception $exception){
            dump($exception);
            throw $exception;
        }

        return true;
    }

    public function update()
    {
        $this->loadUsersFromBeecomeThenUpdate();
    }

    private function loadUsersFromBeecomeThenUpdate()
    {
        foreach (['students', 'speakers', 'admins'] as $type) {
            $response = $this->httpClient->request('GET', $this->beecomeBaseUrl . '/apirest/schools/245/' . $type, [
                'headers' => [
                    'BeecomeKey' => $this->beecomeApiKey
                ]
            ])->getContent();

            $this->updateUsers(json_decode($response, true));
        }
    }

    private function updateUsers(array $usersRaw)
    {
        foreach ($usersRaw as $userRaw) {
            if (!$this->userRepository->findOneBy(['beecomeId' => $userRaw['id']])) {
                $user = $this->userRepository->findOneBy(['email' => $userRaw['email']]) ?? new User();

                $user
                    ->setEmail($userRaw['email'])
                    ->setRoles($userRaw['roles'])
                    ->setBeecomeId($userRaw['id']);

                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }
    }
}